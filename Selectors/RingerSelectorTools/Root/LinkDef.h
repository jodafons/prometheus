
//#include "RingerSelectorTools/IAsgElectronRingerSelector.h"
#include "RingerSelectorTools/ElectronRingerSelector.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//#pragma link C++ class prometheus::IAsgElectronRingerSelector+;
#pragma link C++ class prometheus::ElectronRingerSelector+;


#endif
