
#include "EventAtlas/MuonPhysVal.h"
#include "EventAtlas/RingerPhysVal.h"
#include "EventAtlas/RingerPhysVal_v2.h"
#include "EventAtlas/SkimmedNtuple.h"
#include "EventAtlas/SkimmedNtuple_v2.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#pragma link C++ class edm::RingerPhysVal+;
#pragma link C++ class edm::RingerPhysVal_v2+;
#pragma link C++ class edm::MuonPhysVal+;
#pragma link C++ class edm::SkimmedNtuple+;
#pragma link C++ class edm::SkimmedNtuple_v2+;

#endif
